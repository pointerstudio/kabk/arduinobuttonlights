# how to make a button that turns lights on

here the schematics with a little explanation text:

![schematics](schematics/schematics.drawio.svg)


# useful links

- [Arduino Button tutorial](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button)
- [Arduino LED tutorial](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink)
- [how to measure resistor values with multimeter](https://www.youtube.com/watch?v=1bohzeqWW8I)
