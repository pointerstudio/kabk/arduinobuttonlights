/*
  Simple Arduino Button Lights
*/

// first, let's define the PINs where you want to connect your button and LEDs.
// notice, that in the syntax you do not use a ";" at the end of the line
// here, this is correct! that's because this is kind of like a search/replace instruction.
// arduino will replace every text that matches "PIN_BUTTON" in your code with the number 5,
// before it uploads it to the microcontroller to safe space

#define PIN_BUTTON 5
#define PIN_LED_RED 6
#define PIN_LED_GREEN 7

// the setup function runs once when you press reset or power the board
void setup() {

  // first, let's tell Arduino how we want to use the pins.
  // it needs to know whether it should read from them or write
  // a button wants to be "read", and a LED wants to be "written" to.
  
  // initialize digital pin PIN_BUTTON as an input.
  pinMode(PIN_BUTTON, INPUT);
  
  // initialize digital pin PIN_LED_RED and PIN_LED_GREEN as an output.
  pinMode(PIN_LED_RED, OUTPUT);
  pinMode(PIN_LED_GREEN, OUTPUT);
  
}

// the loop function runs over and over again forever
void loop() {
  // check if button is currently pressed down
  if (digitalRead(PIN_BUTTON) == LOW) {
    // fyi, the following is like an animation
    // the arduino will just follow the steps and ignore any button presses in the meantime
    
    // let's flash the lights quickly to show that we noticed the button press
    digitalWrite(PIN_LED_RED, HIGH);
    digitalWrite(PIN_LED_GREEN, HIGH);
    
    // leave them on for a 1/5 second
    delay(200);
    
    digitalWrite(PIN_LED_RED, LOW);
    digitalWrite(PIN_LED_GREEN, LOW);
    
    delay(200);

    // flash the buttons 100 times
    // to pretend calculating
    for (int i = 0; i < 100; i++) {
      digitalWrite(PIN_LED_RED, HIGH);
      digitalWrite(PIN_LED_GREEN, HIGH);
      
      // leave them on for a 1/5 second
      delay(200);
      
      digitalWrite(PIN_LED_RED, LOW);
      digitalWrite(PIN_LED_GREEN, LOW);

      // leave them off for a 1/5 second
      delay(200);
    }


    // actually calculate a random number between 0 and 100:
    int randomNumber = random(0,100);

    // should the random number be lower than 50, turn the red light on,
    // otherwise the green light
    // btw, by making the threshold higher or lower than 50, you can
    // influence how likely the result is green or red :-)
    if (randomNumber < 50) {
      digitalWrite(PIN_LED_RED, HIGH);
    } else {
      digitalWrite(PIN_LED_GREEN, HIGH);
    }
    
    // leave them on for 20 seconds
    delay(1000 * 20);

    // then turn both lights off again!
    // this last step is actually not necessary,
    // because they will be turned off anyways if the button is not pressed
    digitalWrite(PIN_LED_RED, LOW);
    digitalWrite(PIN_LED_GREEN, LOW);
    
  } else { // it is not pressed down
    // it is, so let's turn the red light off
    digitalWrite(PIN_LED_RED, LOW);
    digitalWrite(PIN_LED_GREEN, LOW);
  }
}

// don't worry, we will get to the green LED in the next sketch
